﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Timer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int _start = 0;
        private int sekunde = 1;
        private int minute = 1;
        private int stunde = 1;
        public int[] binaerSekunden = new int[6];
        public int[] binaerMinuten = new int[6];
        public int[] binaerStunden = new int[6];
        toBinaer binaer = new toBinaer();
        private void timer1_Tick(object sender, EventArgs e)
        {
            
            binaerSekunden = binaer.decimalToBinaer(sekunde);
            binaerMinuten = binaer.decimalToBinaer(minute);
            binaerStunden = binaer.decimalToBinaer(stunde);
            lblSekunge.Text = (Convert.ToString(binaerSekunden[0]) + Convert.ToString(binaerSekunden[1]) + Convert.ToString(binaerSekunden[2]) + Convert.ToString(binaerSekunden[3]) + Convert.ToString(binaerSekunden[4]) + Convert.ToString(binaerSekunden[5]));
            lblMinute.Text = (Convert.ToString(binaerMinuten[0]) + Convert.ToString(binaerMinuten[1]) + Convert.ToString(binaerMinuten[2]) + Convert.ToString(binaerMinuten[3]) + Convert.ToString(binaerMinuten[4]) + Convert.ToString(binaerMinuten[5]));
            lblStunde.Text = (Convert.ToString(binaerStunden[0]) + Convert.ToString(binaerStunden[1]) + Convert.ToString(binaerStunden[2]) + Convert.ToString(binaerStunden[3]) + Convert.ToString(binaerStunden[4]) + Convert.ToString(binaerStunden[5]));
            sekunde++;
            if (sekunde == 60)
            {
                sekunde = 0;
                minute++;
                if (minute == 60)
                {
                    minute = 0;
                    stunde++;
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            _start++;
            if (_start % 2 == 0)
            {
                timer1.Stop();
            }
            else
            {
                timer1.Start();
            }
        }
    }

    public class toBinaer
    {
        public int[] decimalToBinaer(int dezimal)
        {
            var binerdigit = 0;
            int[] zahlen = new int[6] {0, 0, 0, 0, 0, 0};
            var solved = true;
            while (solved)
            {
                for (int i = 5; i > 0; i--)
                {
                    binerdigit = dezimal % 2;
                    dezimal = dezimal / 2;
                    zahlen[i] = binerdigit;
                    if (dezimal == 0)
                    {
                        i = 0;
                    }
                }
                solved = false;
            }
            return zahlen;
        }
    }
}