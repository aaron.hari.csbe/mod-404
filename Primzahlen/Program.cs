﻿using System;
using System.Collections;
using System.ComponentModel;

namespace M404_PrimzahlenAusrechnen
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Boolean firstParameter = true;
            Boolean secondParameter = true;
            
            while(firstParameter)
            {
                secondParameter = true;
                Console.Write("Welche Zahl möchten Sie pruefen? ");
                var numberToTest = Convert.ToInt32(Console.ReadLine());

                double primesToTestUpTo = Math.Sqrt(numberToTest);
                //Console.WriteLine(primesToTestUpTo);

                Int64 divider = 1;

                while(secondParameter)
                {
                    divider++;
                    if ((numberToTest % divider) == 0)
                    {
                        Console.WriteLine(numberToTest + " ist keine Primzahl");
                        secondParameter = false;
                    }
                    else if (divider > primesToTestUpTo)
                    {
                        Console.WriteLine(numberToTest + " ist eine Primzahl");
                        secondParameter = false;
                    }
                }

            }
            
            
        }
    }
}