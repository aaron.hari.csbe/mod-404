﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Taschenrechner02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void reset(double ergebnis)
        {
            lblErgebnis.Text = Convert.ToString(ergebnis);
            txtOperand1.Text = Convert.ToString(ergebnis);
            txtOperand2.Text = "";
        }

        public double[] input()
        {
            try
            {
                var zahl1 = Convert.ToDouble(txtOperand1.Text);
                var zahl2 = Convert.ToDouble(txtOperand2.Text);
                double[] zahlen = {zahl1, zahl2};
                return zahlen;
            }
            catch (Exception e)
            {
                txtOperand2.Text = "0";
                return new double[2];
            }
        }
        
        
        private void btnAddition_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Addition(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "+";
        }

        private void btnSubtraktion_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Subtraktion(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "-";
        }

        private void btnDurchschnitt_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Durchschnitt(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "Ø";
        }

        private void btnPotenz_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Potenz(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "^";
        }

        private void btnMaximum_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Maximum(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "Ø";
        }

        private void btnDivision_Click(object sender, EventArgs e)
        {
            double[] zahlen = input();
            var rechnen = new Rechnen();
            double ergebnis = rechnen.Division(zahlen);
            lblErgebnis.Text = Convert.ToString(ergebnis);
            reset(ergebnis);
            lblOperator.Text = "/";
        }
    }
}
