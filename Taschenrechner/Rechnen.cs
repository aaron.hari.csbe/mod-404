﻿using System;

namespace Taschenrechner02
{
    public partial class Rechnen
    {
        public double Addition(double[] zahlen)
        {
            return zahlen[0] + zahlen[1];
        }
        public double Subtraktion(double[] zahlen)
        {
            return zahlen[0] - zahlen[1];
        }
        public double Durchschnitt(double[] zahlen)
        {
            return (zahlen[0] + zahlen[1]) / 2;
        }
        public double Potenz(double[] zahlen)
        {
            return Math.Pow(zahlen[0], zahlen[1]);
        }
        public double Maximum(double[] zahlen)
        {
            if (zahlen[0] > zahlen[1])
            {
                return zahlen[0];
            }
            return zahlen[1];
        }
        public double Division(double[] zahlen)
        {
            return zahlen[0] / zahlen[1];
        }
    }
}