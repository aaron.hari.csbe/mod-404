﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Ping_Pong01
{
    public partial class GameOverFrom : Form
    {
        string scoreText;
        public string punkte2;
        string fileName = @"score\score.txt";
        public GameOverFrom()
        {
            InitializeComponent();
        }
        public  void GameOverFrom_Load(object sender, EventArgs e)
        {
            try
            {
                if (!File.Exists(fileName))
                {
                    FileStream createFile = File.Create(fileName);
                    createFile.Close();
                }
                if (File.Exists(fileName))
                {
                    StreamReader readFile = File.OpenText(fileName);
                    scoreText = readFile.ReadToEnd();
                    readFile.Close();
                    lblErgebnis.Text = scoreText;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
            lblPunkte.Text = punkte2;
        }


        private void btnEintragen_Click(object sender, EventArgs e)
        {
            if (txtName.Text != null)
            {
                var name = txtName.Text;
                var date = DateTime.Now;
                var punkte = Convert.ToString(punkte2);
                var writeFile = new StreamWriter(fileName);
                writeFile.WriteLine(scoreText + " " + punkte + " " + name + " " + date);
                writeFile.Close();
                
                var readFile = File.OpenText(fileName);
                scoreText = readFile.ReadToEnd();
                readFile.Close();
                lblErgebnis.Text = scoreText;
            }
        }

        private void btnSchliessen_Click(object sender, EventArgs e)
        {
            Close();
            
        }
    }
}