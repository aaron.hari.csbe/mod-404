﻿using System.ComponentModel;

namespace Ping_Pong01
{
    partial class GameOverFrom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSchliessen = new System.Windows.Forms.Button();
            this.btnEintragen = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblErgebnis = new System.Windows.Forms.Label();
            this.lblPunkte = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSchliessen
            // 
            this.btnSchliessen.Location = new System.Drawing.Point(265, 270);
            this.btnSchliessen.Name = "btnSchliessen";
            this.btnSchliessen.Size = new System.Drawing.Size(101, 30);
            this.btnSchliessen.TabIndex = 13;
            this.btnSchliessen.Text = "Zurück";
            this.btnSchliessen.UseVisualStyleBackColor = true;
            this.btnSchliessen.Click += new System.EventHandler(this.btnSchliessen_Click);
            // 
            // btnEintragen
            // 
            this.btnEintragen.Location = new System.Drawing.Point(265, 39);
            this.btnEintragen.Name = "btnEintragen";
            this.btnEintragen.Size = new System.Drawing.Size(101, 30);
            this.btnEintragen.TabIndex = 12;
            this.btnEintragen.Text = "Eintragen";
            this.btnEintragen.UseVisualStyleBackColor = true;
            this.btnEintragen.Click += new System.EventHandler(this.btnEintragen_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(133, 40);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(97, 23);
            this.txtName.TabIndex = 11;
            // 
            // lblErgebnis
            // 
            this.lblErgebnis.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.lblErgebnis.Location = new System.Drawing.Point(12, 87);
            this.lblErgebnis.Name = "lblErgebnis";
            this.lblErgebnis.Size = new System.Drawing.Size(353, 170);
            this.lblErgebnis.TabIndex = 10;
            // 
            // lblPunkte
            // 
            this.lblPunkte.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPunkte.Location = new System.Drawing.Point(12, 39);
            this.lblPunkte.Name = "lblPunkte";
            this.lblPunkte.Size = new System.Drawing.Size(63, 30);
            this.lblPunkte.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(133, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(142, 28);
            this.label2.TabIndex = 8;
            this.label2.Text = "Name:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 30);
            this.label1.TabIndex = 7;
            this.label1.Text = "Puntke:";
            // 
            // GameOverFrom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 314);
            this.Controls.Add(this.btnSchliessen);
            this.Controls.Add(this.btnEintragen);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblErgebnis);
            this.Controls.Add(this.lblPunkte);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GameOverFrom";
            this.Text = "GameOverFrom";
            this.Load += new System.EventHandler(this.GameOverFrom_Load);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblErgebnis;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button btnEintragen;
        private System.Windows.Forms.Button btnSchliessen;
        private System.Windows.Forms.Label lblPunkte;
    }
}