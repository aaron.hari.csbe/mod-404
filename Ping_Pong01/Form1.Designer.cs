﻿namespace Ping_Pong01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlSpiel = new System.Windows.Forms.Panel();
            this.picSchlaegerRechts = new System.Windows.Forms.PictureBox();
            this.picBall = new System.Windows.Forms.PictureBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.tmrSpiel = new System.Windows.Forms.Timer(this.components);
            this.vbsSchlaegerRechts = new System.Windows.Forms.VScrollBar();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPunkte = new System.Windows.Forms.TextBox();
            this.btnHoch = new System.Windows.Forms.Button();
            this.btnLinks = new System.Windows.Forms.Button();
            this.btnRechts = new System.Windows.Forms.Button();
            this.btnRunter = new System.Windows.Forms.Button();
            this.lblSteuerung = new System.Windows.Forms.Label();
            this.grpSteuerung = new System.Windows.Forms.GroupBox();
            this.rdbSchlaeger = new System.Windows.Forms.RadioButton();
            this.rdbBall = new System.Windows.Forms.RadioButton();
            this.pnlSpiel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.picSchlaegerRechts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).BeginInit();
            this.grpSteuerung.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSpiel
            // 
            this.pnlSpiel.BackColor = System.Drawing.Color.SeaGreen;
            this.pnlSpiel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlSpiel.Controls.Add(this.picSchlaegerRechts);
            this.pnlSpiel.Controls.Add(this.picBall);
            this.pnlSpiel.Location = new System.Drawing.Point(49, 72);
            this.pnlSpiel.Name = "pnlSpiel";
            this.pnlSpiel.Size = new System.Drawing.Size(299, 200);
            this.pnlSpiel.TabIndex = 0;
            // 
            // picSchlaegerRechts
            // 
            this.picSchlaegerRechts.BackColor = System.Drawing.Color.Black;
            this.picSchlaegerRechts.Location = new System.Drawing.Point(274, 72);
            this.picSchlaegerRechts.Name = "picSchlaegerRechts";
            this.picSchlaegerRechts.Size = new System.Drawing.Size(3, 40);
            this.picSchlaegerRechts.TabIndex = 1;
            this.picSchlaegerRechts.TabStop = false;
            // 
            // picBall
            // 
            this.picBall.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.picBall.Location = new System.Drawing.Point(61, 60);
            this.picBall.Name = "picBall";
            this.picBall.Size = new System.Drawing.Size(24, 25);
            this.picBall.TabIndex = 0;
            this.picBall.TabStop = false;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(49, 303);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(79, 42);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Spiel starten";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click_1);
            // 
            // tmrSpiel
            // 
            this.tmrSpiel.Interval = 80;
            this.tmrSpiel.Tick += new System.EventHandler(this.tmrSpiel_Tick);
            // 
            // vbsSchlaegerRechts
            // 
            this.vbsSchlaegerRechts.Location = new System.Drawing.Point(351, 54);
            this.vbsSchlaegerRechts.Maximum = 146;
            this.vbsSchlaegerRechts.Name = "vbsSchlaegerRechts";
            this.vbsSchlaegerRechts.Size = new System.Drawing.Size(37, 237);
            this.vbsSchlaegerRechts.TabIndex = 2;
            this.vbsSchlaegerRechts.Value = 50;
            this.vbsSchlaegerRechts.Scroll +=
                new System.Windows.Forms.ScrollEventHandler(this.vbsSchlaegerRechts_Scroll);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(49, 278);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 22);
            this.label1.TabIndex = 3;
            this.label1.Text = "Punkte:";
            // 
            // txtPunkte
            // 
            this.txtPunkte.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.txtPunkte.Location = new System.Drawing.Point(99, 275);
            this.txtPunkte.Name = "txtPunkte";
            this.txtPunkte.Size = new System.Drawing.Size(49, 27);
            this.txtPunkte.TabIndex = 4;
            this.txtPunkte.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnHoch
            // 
            this.btnHoch.BackColor = System.Drawing.Color.White;
            this.btnHoch.Image = ((System.Drawing.Image) (resources.GetObject("btnHoch.Image")));
            this.btnHoch.Location = new System.Drawing.Point(551, 46);
            this.btnHoch.Name = "btnHoch";
            this.btnHoch.Size = new System.Drawing.Size(106, 138);
            this.btnHoch.TabIndex = 5;
            this.btnHoch.Tag = "ho";
            this.btnHoch.UseVisualStyleBackColor = false;
            this.btnHoch.Click += new System.EventHandler(this.btnHoch_Click);
            // 
            // btnLinks
            // 
            this.btnLinks.BackColor = System.Drawing.Color.White;
            this.btnLinks.Image = ((System.Drawing.Image) (resources.GetObject("btnLinks.Image")));
            this.btnLinks.Location = new System.Drawing.Point(407, 192);
            this.btnLinks.Name = "btnLinks";
            this.btnLinks.Size = new System.Drawing.Size(139, 106);
            this.btnLinks.TabIndex = 6;
            this.btnLinks.Tag = "li";
            this.btnLinks.UseVisualStyleBackColor = false;
            this.btnLinks.Click += new System.EventHandler(this.btnLinks_Click);
            // 
            // btnRechts
            // 
            this.btnRechts.BackColor = System.Drawing.Color.White;
            this.btnRechts.Image = ((System.Drawing.Image) (resources.GetObject("btnRechts.Image")));
            this.btnRechts.Location = new System.Drawing.Point(663, 192);
            this.btnRechts.Name = "btnRechts";
            this.btnRechts.Size = new System.Drawing.Size(139, 106);
            this.btnRechts.TabIndex = 7;
            this.btnRechts.Tag = "re";
            this.btnRechts.UseVisualStyleBackColor = false;
            this.btnRechts.Click += new System.EventHandler(this.btnRechts_Click);
            // 
            // btnRunter
            // 
            this.btnRunter.BackColor = System.Drawing.Color.White;
            this.btnRunter.Image = ((System.Drawing.Image) (resources.GetObject("btnRunter.Image")));
            this.btnRunter.Location = new System.Drawing.Point(551, 303);
            this.btnRunter.Name = "btnRunter";
            this.btnRunter.Size = new System.Drawing.Size(106, 138);
            this.btnRunter.TabIndex = 8;
            this.btnRunter.Tag = "ru";
            this.btnRunter.UseVisualStyleBackColor = false;
            this.btnRunter.Click += new System.EventHandler(this.btnRunter_Click);
            // 
            // lblSteuerung
            // 
            this.lblSteuerung.Location = new System.Drawing.Point(134, 305);
            this.lblSteuerung.Name = "lblSteuerung";
            this.lblSteuerung.Size = new System.Drawing.Size(231, 61);
            this.lblSteuerung.TabIndex = 9;
            this.lblSteuerung.Text =
                "H --> Horizontale Flugrichtung umkehren\r\nV -->  Vertikale Flugrichtung umkehren\r\n" +
                "P -->  Speil pausieren\r\nS -->  Speil weirer laufen lassen";
            // 
            // grpSteuerung
            // 
            this.grpSteuerung.Controls.Add(this.rdbSchlaeger);
            this.grpSteuerung.Controls.Add(this.rdbBall);
            this.grpSteuerung.Location = new System.Drawing.Point(387, 378);
            this.grpSteuerung.Name = "grpSteuerung";
            this.grpSteuerung.Size = new System.Drawing.Size(157, 99);
            this.grpSteuerung.TabIndex = 10;
            this.grpSteuerung.TabStop = false;
            this.grpSteuerung.Text = "Auswahl der Steuerung";
            // 
            // rdbSchlaeger
            // 
            this.rdbSchlaeger.Checked = true;
            this.rdbSchlaeger.Location = new System.Drawing.Point(15, 54);
            this.rdbSchlaeger.Name = "rdbSchlaeger";
            this.rdbSchlaeger.Size = new System.Drawing.Size(136, 39);
            this.rdbSchlaeger.TabIndex = 1;
            this.rdbSchlaeger.TabStop = true;
            this.rdbSchlaeger.Text = "Schlägersteuerung";
            this.rdbSchlaeger.UseVisualStyleBackColor = true;
            this.rdbSchlaeger.CheckedChanged += new System.EventHandler(this.rdbSchlaeger_CheckedChanged);
            // 
            // rdbBall
            // 
            this.rdbBall.Location = new System.Drawing.Point(13, 30);
            this.rdbBall.Name = "rdbBall";
            this.rdbBall.Size = new System.Drawing.Size(99, 17);
            this.rdbBall.TabIndex = 0;
            this.rdbBall.Text = "Ballsteuerung";
            this.rdbBall.UseVisualStyleBackColor = true;
            this.rdbBall.CheckedChanged += new System.EventHandler(this.rdbBall_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.grpSteuerung);
            this.Controls.Add(this.lblSteuerung);
            this.Controls.Add(this.btnRunter);
            this.Controls.Add(this.btnRechts);
            this.Controls.Add(this.btnLinks);
            this.Controls.Add(this.btnHoch);
            this.Controls.Add(this.txtPunkte);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.vbsSchlaegerRechts);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.pnlSpiel);
            this.Name = "Form1";
            this.Text = "Ping-Pong Spiel";
            this.pnlSpiel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.picSchlaegerRechts)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.picBall)).EndInit();
            this.grpSteuerung.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Panel pnlSpiel;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.PictureBox picBall;
        private System.Windows.Forms.Timer tmrSpiel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.VScrollBar vbsSchlaegerRechts;
        private System.Windows.Forms.PictureBox picSchlaegerRechts;
        private System.Windows.Forms.TextBox txtPunkte;
        private System.Windows.Forms.Button btnHoch;
        private System.Windows.Forms.Button btnLinks;
        private System.Windows.Forms.Button btnRunter;
        private System.Windows.Forms.Button btnRechts;
        private System.Windows.Forms.Label lblSteuerung;
        private System.Windows.Forms.GroupBox grpSteuerung;
        private System.Windows.Forms.RadioButton rdbBall;
        private System.Windows.Forms.RadioButton rdbSchlaeger;
    }
}