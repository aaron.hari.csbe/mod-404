﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ping_Pong01
{
    public partial class Form1 : Form
    {
        private Int32 xStart = 60;
        private Int32 yStart = 60;
            
        Int32 xAdd = 5;
        Int32 yAdd = 2;

        bool mode = true;

        public static int punktZahl = 0;
        
        public Form1()
        {
            InitializeComponent();
        }

        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.P)
            {
                tmrSpiel.Stop();
                return true;
            }
            
            if (keyData == Keys.S)
            {
                tmrSpiel.Start();
                return true;
            }
            
            if (keyData == Keys.H)
            {
                if (yAdd == 2)
                {
                    yAdd = -2;
                }

                if (yAdd == -2)
                {
                    yAdd = 2;
                }
                return true;
            }
            
            if (keyData == Keys.V)
            {
                if (xAdd == 5)
                {
                    xAdd = -5;
                }

                if (xAdd == -5)
                {
                    xAdd = 5;
                }
                return true;
            }
            
            return base.ProcessDialogKey(keyData);
        }

        
        private void tmrSpiel_Tick(object sender, EventArgs e)
        {
           
            

            picBall.Location = new Point(picBall.Location.X + xAdd, picBall.Location.Y + yAdd);
            
            if (picBall.Location.X >= (pnlSpiel.Width - 25))
            {
                tmrSpiel.Stop();
                var frm2 = new GameOverFrom();
                frm2.punkte2 = "" + punktZahl;
                frm2.Show();
                picBall.Location = new Point(xStart, yStart);
                punktZahl = 0;
                tmrSpiel.Interval = 80;
                picBall.BackColor = Color.DeepSkyBlue;

            }
            if (picBall.Location.X <= 0)
            {
                xAdd = 5;
            }
            
            if (picBall.Location.Y >= (pnlSpiel.Height - 25))
            {
                yAdd = -2;
            }
            if (picBall.Location.Y <= 0)
            {
                yAdd = 2;
            }

            if (picBall.Location.X + picBall.Width >= picSchlaegerRechts.Location.X)
            {
                for (int i = 0; i < (picBall.Height + picSchlaegerRechts.Height) && xAdd != -5; i++)
                {
                    if (picBall.Location.Y > (picSchlaegerRechts.Location.Y - picBall.Height) && picBall.Location.Y < (picSchlaegerRechts.Location.Y + picSchlaegerRechts.Height + picBall.Height))
                    {
                        punktZahl = punktZahl + 10;
                        xAdd = -5;
                    }
                }
                if (punktZahl > 0 && punktZahl % 50 == 0)
                {
                    if (tmrSpiel.Interval > 10)
                    {
                        tmrSpiel.Interval = tmrSpiel.Interval - 10;
                    }
                   else if (tmrSpiel.Interval > 1)
                    {
                        tmrSpiel.Interval = tmrSpiel.Interval - 2;
                    }
                    var random = new Random();
                    int randomNumber = random.Next(1, 10);
                    switch (randomNumber)
                    {
                        case 1:
                            picBall.BackColor = Color.Aqua;
                            break;
                        case 2:
                            picBall.BackColor = Color.Brown;
                            break;
                        case 3:
                            picBall.BackColor = Color.Chartreuse;
                            break;
                        case 4:
                            picBall.BackColor = Color.Fuchsia;
                            break;
                        case 5:
                            picBall.BackColor = Color.Goldenrod;
                            break;
                        case 6:
                            picBall.BackColor = Color.Teal;
                            break;
                        case 7:
                            picBall.BackColor = Color.DimGray;
                            break;
                        case 8:
                            picBall.BackColor = Color.YellowGreen;
                            break;
                        case 9:
                            picBall.BackColor = Color.DarkOliveGreen;
                            break;
                        case 10:
                            picBall.BackColor = Color.LightSlateGray;
                            break;
                    }

                }
            }

            txtPunkte.Text = Convert.ToString(punktZahl);
        }
        
        private void btnStart_Click_1(object sender, EventArgs e)
        {
            tmrSpiel.Start();
        }
        
        private void vbsSchlaegerRechts_Scroll(object sender, ScrollEventArgs e)
        {
            if (mode)
            {
                picSchlaegerRechts.Location = new Point(picSchlaegerRechts.Location.X, (vbsSchlaegerRechts.Value));
            }
        }

        private void btnHoch_Click(object sender, EventArgs e)
        {
            if (picBall.Location.Y < 25)
            {
                picBall.Location = new Point(picBall.Location.X, 0);
            }
            else
            {
                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y - 25); 
            }
        }

        private void btnRechts_Click(object sender, EventArgs e)
        {
            picBall.Location = new Point(picBall.Location.X + 25, picBall.Location.Y);
        }

        private void btnRunter_Click(object sender, EventArgs e)
        {
            if (picBall.Location.Y > pnlSpiel.Height - picBall.Height - 1)
            {
                picBall.Location = new Point(picBall.Location.X, pnlSpiel.Height - picBall.Height);
            }
            else
            {
                picBall.Location = new Point(picBall.Location.X, picBall.Location.Y + 25);
            }
        }


        private void btnLinks_Click(object sender, EventArgs e)
        {
            if (picBall.Location.X < 25)
            {
                picBall.Location = new Point(0, picBall.Location.Y);
            }
            else
            {
                picBall.Location = new Point(picBall.Location.X - 25, picBall.Location.Y);
            }
        }


        private void rdbBall_CheckedChanged(object sender, EventArgs e)
        {
            vbsSchlaegerRechts.Enabled = false;
            mode = false;
        }

        private void rdbSchlaeger_CheckedChanged(object sender, EventArgs e)
        {
            vbsSchlaegerRechts.Enabled = true;
            mode = true;
        }
    }
}