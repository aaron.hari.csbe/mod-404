﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace schachbrett
{
    public partial class Form1 : Form
    {
        private void newPicBox(int count, int x, int y, int width, int height)
        {
            //btnStart.Text = pnlSchachbrett.Width.ToString();
            var name = "picBox" + Convert.ToString(count);
            var picBox = new PictureBox
            {
                Name = name,
                Location = new Point(x, y),
                Size = new Size(width, height),
                BackColor = Color.Black
            };
            //picBox.BringToFront();
            pnlSchachbrett.Controls.Add(picBox);
        }
        public Form1()
        {
            InitializeComponent();
        }
        private void btnStart_Click(object sender, EventArgs e)
        {
            lblHint.Text = "";
            pnlSchachbrett.Controls.Clear();
            pnlSchachbrett.Width = 300;
            pnlSchachbrett.Height = 300;
            if (nudAnzahlZeilen.Value % 2 != 0)
            {
                lblHint.Text = "Hinweis: Das Programm unterstütz nur gerade Zahlen.";
                nudAnzahlZeilen.Value = 0;
                pnlSchachbrett.Controls.Add(lblHint);
            }
            else
            {
                var columns = Convert.ToInt32(nudAnzahlZeilen.Value);
                var width = pnlSchachbrett.Width / columns;
                var height = width;
                var x = width * -2;
                var y = 0;
                var rows = 0;
                pnlSchachbrett.Width = width * columns;
                pnlSchachbrett.Height = pnlSchachbrett.Width;

                for (var i = 0; i < Math.Pow(columns, 2); i++)
                {
                    x += width * 2;
                    if (i % (columns / 2) == 0 && i != 0)
                    {
                        rows++;
                        y += height;
                        x -= columns * width;
                        if (rows % 2 == 0 && rows != 0)
                        {
                            x -= width;
                        }
                        else
                        {
                            x += width;
                        }
                    }

                    newPicBox(i, x, y, width, height);
                }
            }
        }
    }
}